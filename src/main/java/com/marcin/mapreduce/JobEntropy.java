package com.marcin.mapreduce;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class JobEntropy implements Tool{
   
    private Configuration conf;
    
    public static void runJob(String[] args) throws Exception{
        int status = ToolRunner.run(new Configuration(), new JobEntropy(), args);
        System.out.println("Status: " + status);
    }
   

    @Override
    public int run(String[] args) throws Exception {
        Configuration conf = this.prepareConfiguration(null);
        conf = this.prepareConfiguration(conf);
        FileSystem fs = FileSystem.get(conf);
        if (fs.exists(new Path("output"))) {
            fs.delete(new Path("output"), true);
        }
        Job job = Job.getInstance(conf);
        job.setJobName("Entropy");
        job.setJarByClass(this.getClass());
       
        job.setMapperClass(MapperEntropy.class);
        job.setReducerClass(ReducerEntropy.class);
        job.setSortComparatorClass(SortEntropy.class);//SortEntropy.class
       
        //job.setMapOutputKeyClass(Text.class);
        //job.setMapOutputValueClass(DoubleWritable.class);
       
        job.setOutputKeyClass(DoubleWritable.class);
        job.setOutputValueClass(Text.class);
       
        //job.setInputFormatClass(TextInputFormat.class);
        //job.setOutputFormatClass(TextOutputFormat.class);
       
        FileInputFormat.addInputPath(job, new Path("input"));
        FileOutputFormat.setOutputPath(job, new Path("output"));
       
        return job.waitForCompletion(true) ? 0 : -1;
    }

    @Override
    public void setConf(Configuration conf) {
        this.conf = conf;
    }

    @Override
    public Configuration getConf() {
        return conf;
    }
    
        public Configuration prepareConfiguration(Configuration conf) {
        if(conf == null)
            conf = new Configuration();
        System.setProperty("HADOOP_USER_NAME", "vagrant");
        conf.set("mapred.job.tracker", "192.168.5.10:8021");
        conf.set("fs.default.name", "hdfs://192.168.5.10:9000/user/vagrant");
        conf.set("hadoop.job.ugi", "vagrant");

        return conf;
    }
}
